var sanat = ["GATE", "WAY", "TECHNO", "LABS", "DEVEL", "OPER"];
var ryhma0 = ["ABCDEF", "GHIJKL", "ERPHJJ", "EDETDQ"];
var ryhma1 = ["DFGADF", "FIHOEF", "DOIWYH", "FUFWYD"];
var ryhma2 = ["HFOEIH", "IFTEOI", "ICHOEH", "INFOEH"];
var ryhma3 = ["LABSDF", "BCDEFG", "IHFOEI", "IHFOEF"];
var ryhma4 = ["IOHFED", "RIFHEW", "IEOHFD", "EOVIUE", "HULILA", "EEEEEE"];
var ryhma5 = ["AAAAAA", "OPPPPP", "EEEEEE", "RRRRRR", "ABCDEF"];

String.prototype.contains = function(str) { return this.indexOf(str) != -1; };

/*
 * Funktiolle annetaan parametrina haluttua sanaa vastaava indeksi, sek� sit� vastaava Ryhm�
 */
function tarkistaSana(sana,ryhma){

var ryhmat = ryhma;
var ryhmaP = ryhmat.length;
var sana = sana;
var muodostus = "";
var j=1; // Apumuuttuja verratavan kirjaimen indeksi +1
var k=0; // Verrattavan ryhm�n muuttuja
var z=0; // Verrattavan kirjaimen indeksi
var tarkistetut = []; // Lista johon lis�t��n ryhm�, kun siit� on haettu sanan haettavaa kirjainta

//Tarkistetaan ett� ryhmi� on riitt�v� m��r� sanan muodostamiseen
if(ryhmat.length<sana.length){
  console.log("Sana: ", sana, "Tulos: Ei mahdollinen (Liian v�h�n ryhmi�)")
  return;
}  
  
// Haetaan sanan kirjaimia ryhmist� kunnes sana on muodostettu.
// Mik�li sanaa ei voida muodostaa annetuista ryhmist� ja ilmoitetaan k�ytt�j�lle
while(sana!=muodostus){
  if(tarkistetut.length == ryhmaP){
    console.log("Sana: ",sana, "Tulos: Ei mahdollinen (Ryhmist� ei l�ytynyt tarvittavia kirjaimia)")
    return;
  }
if(k>=ryhmat.length)
    {k=0}
if(ryhmat[k].contains(sana.substring(z,j))){
  	muodostus += sana.substring(z,j);
 	ryhmat.splice(k,1);
  	z++;
  	j++;
  	k=0;
}else{
  tarkistetut.push(ryhmat[k])
  k++;
}}
  console.log("Sana:",sana, ", Tulos: Mahdollinen")
}

//--------------------TESTAUS--------------------------
// GATE
tarkistaSana(sanat[0],ryhma0);
// WAY
tarkistaSana(sanat[1],ryhma1);
// TECHNO
tarkistaSana(sanat[2],ryhma2);
// LABS
tarkistaSana(sanat[3],ryhma3);
// DEVEL
tarkistaSana(sanat[4],ryhma4);
// OPER
tarkistaSana(sanat[5],ryhma5);